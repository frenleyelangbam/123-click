<?php 
if ($os_type == 'ios') {
                    $sub_sub_query = $mysqli->query("SELECT firstname from playat_user where id='$userid'");

                    if ($sub_sub_query->num_rows > 0) {
                        $row = $sub_sub_query->fetch_assoc();

                        $firstname = $row['firstname'];



                        $deviceToken = $device_token;

                        $passphrase = '';

                        // Put your alert message here:
                        $message = '' . $firstname . ' has joined ' . $eventname . ' game.';


                        ////////////////////////////////////////////////////////////////////////////////

                        $ctx = stream_context_create();
                        stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushcert.pem');
                        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                        // Open a connection to the APNS server
                        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);


                        if (!$fp)
                            exit("Failed to connect: $err $errstr" . PHP_EOL);

                        //echo 'Connected to APNS' . PHP_EOL;
                        // Create the payload body
                        // $body['aps'] = array(
                        // 'alert' => array(
                        //  'body' => $message,$id,
                        // //'action-loc-key' => 'Bango App',
                        //  ),
                        // 'badge' => 0,
                        // 'sound' => 'oven.caf',
                        // );

                        $body['aps'] = array(
                            'alert' => array(
                                'eventid' => $id,
                                'body' => $message,
                            ),
                            'badge' => 0,
                            'sound' => 'default',
                        );


                        // Encode the payload as JSON
                        $payload = json_encode($body);

                        // Build the binary notification
                        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                        // Send it to the server
                        $result = fwrite($fp, $msg, strlen($msg));
                    }
                }
?>