<?php

/*
function name: send_ios_notification
@param: deviceToken
@param: message
*/

function send_ios_notification($deviceToken,$message){
	$passphrase = '123-clic';
	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', '123ClicPush_Dev.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
	// Open a connection to the APNS server
	$fp = stream_socket_client(
	'ssl://gateway.sandbox.push.apple.com:2195', $err,  // For development
	// 'ssl://gateway.push.apple.com:2195', $err, // for production
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);
	//echo 'Connected to APNS' . PHP_EOL;
	// Create the payload body
	$body['aps'] = array('alert' => trim($message),'sound' =>'default');
	// Encode the payload as JSON
	$payload = json_encode($body);
	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', trim($deviceToken)) . pack('n', strlen($payload)) . $payload;
	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));
	if (!$result){
		echo 'Message not delivered' . PHP_EOL;
	}
	else
	{
	echo 'Message successfully delivered' . PHP_EOL;
	return $result;
	}
	// Close the connection to the server
	fclose($fp);
}




//  Now you can use bellow php function to send IOS push notification
// My device token
$deviceToken = '8AC17E72FF863F6FBD5E180A6F531A58B57B4BA48B8F88ED650848DB0EA41B00';
// My message
$message = 'My first push notification!';
$result = send_ios_notification($deviceToken,$message);
// Debug your result
print_r($result);die;

?>