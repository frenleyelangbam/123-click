<?php 
include('constants.php');
#reminder listing api
$userId = $data['userId'];

$reservationListDetails = array();

$passedReservationDetails = array();
$reminder_status = '';
$reminder_global = '';

$reservationListQuery = mysqli_query($con,"SELECT b.* from `add_booking_new` b  where b.user_id=$userId and b.status='1' order by b.date_booking DESC");


$current_date_time = strtotime(date("Y-m-d H:i:s"));



if(mysqli_num_rows($reservationListQuery) > 0){

	while($total_reservation_array=mysqli_fetch_array($reservationListQuery)){
            $booking_id = $total_reservation_array['booking_id'];
            $dateOfBooking = $total_reservation_array['date_booking'];
            
            $startTimeOfBooking = $total_reservation_array['start']; 
            $datetime = explode(" ",$startTimeOfBooking);
            //$date = $datetime[0];
            $timeOfBooking = $datetime[1];

            
            if($total_reservation_array['reminder_status'] == '' || $total_reservation_array['reminder_status'] == null){
                  $reminder_status = 1;// set default as 1
            }else{
                  $reminder_status = $total_reservation_array['reminder_status'];
            }


            if($total_reservation_array['global_reminder'] == '' || $total_reservation_array['global_reminder'] == null){
                  $reminder_global = 1;// set default as 1
            }else{
                  $reminder_global = $total_reservation_array['global_reminder'];
            }
                       
            $date_of_booking = strtotime($dateOfBooking." ".$timeOfBooking);

            $specialist_id = $total_reservation_array['spe_id'];
            $specialist_data_query = mysqli_query($con,"select * from specialist where spr_id='$specialist_id'");
            $specilaist_data = mysqli_fetch_array($specialist_data_query);

            $service_id = $total_reservation_array['service_id'];
            $service_data_query = mysqli_query($con,"select * from service where serv_title='$service_id' and spe_id='$specialist_id'");

            $service_data = mysqli_fetch_array($service_data_query);
            $service_name = $service_data['serv_title']; // id 
            $servicedetail_data_query = mysqli_query($con,"select * from service_detail where ser_detail_id='$service_name'");
            $servicedetail_data = mysqli_fetch_array($servicedetail_data_query);
            if($current_date_time  > $date_of_booking){

                  $passedReservationDetailsInside['reservation_id'] = $booking_id;
            	$passedReservationDetailsInside['reservation_date'] = $dateOfBooking;
            	$passedReservationDetailsInside['reservation_hour'] = $timeOfBooking;
            	$passedReservationDetailsInside['reservation_professionel'] = $specilaist_data['brand_name'];
                  $passedReservationDetailsInside['reservation_brand_logo'] = SPECIALIST_IMAGE_PATH.$specilaist_data['brand_logo'];
            	$passedReservationDetailsInside['reservation_address'] = $specilaist_data['address'];
            	$passedReservationDetailsInside['reservation_contact'] = $specilaist_data['contact'];
            	$passedReservationDetailsInside['reservation_service'] = $servicedetail_data['ser_name'];
            	$passedReservationDetailsInside['reservation_service_price'] = $service_data['price'];
                  $passedReservationDetailsInside['reservation_reminder_status'] = $reminder_status['reminder_status'];  

                  // $passedReservationDetailsInside['reservation_reminder_global'] = $reminder_global;                   

            	array_push($passedReservationDetails,$passedReservationDetailsInside);

            }/*else if($date_of_booking >= $current_date_time){
                  // upcoming query starts here
                  $upcomingListQuery = mysqli_query($con,"SELECT b.* from `add_booking_new` b  where b.user_id=$userId and b.status='1' order by b.date_booking DESC LIMIT 1");
                  if(@mysqli_num_rows($upcomingListQuery) >0){
                        $reminder_global = @mysqli_fetch_array($upcomingListQuery['global_reminder']);
                  }else{
                        $reminder_global = 1;
                  }
                  // upcoming query ends here

            } */
            
            $datatext['status'] = true;
		$datatext['message'] = "Passed Reservations Successfully Listed.";
		$reservationListDetails ['passedReservationDetails'] = $passedReservationDetails;
		$logParameters = array(
			"Request_Remote_Address" => $remoteAddress,
			"Requested_Page" => 'get_reminders',
			"Request_Method" => $requestType,
			"Request_Sent_From" => $deviceType,
			"Requested_Date_Time" => date('Y-m-d h:i:s'),
			"Request_Status" => 'success',
			"Actual_Data_Received" => $json,
			"Data_Responded" => $reservationListDetails
		);
		$logs->create_log($logParameters,'customer');
		$datatext['details'] = $reservationListDetails;
            $datatext['global_reminder'] = $reminder_global;
    }
} else {
	$datatext['status'] = false;
	$datatext['message'] = "No Reservation Found.";
	$logParameters = array(
			"Request_Remote_Address" => $remoteAddress,
			"Requested_Page" => 'get_reminders',
			"Request_Method" => $requestType,
			"Request_Sent_From" => $deviceType,
			"Requested_Date_Time" => date('Y-m-d h:i:s'),
			"Request_Status" => 'failed',
			"Actual_Data_Received" => $data,
			"Data_Responded" => "NA"
	);
	$logs->create_log($logParameters,'customer');
}

echo json_encode($datatext);

?>