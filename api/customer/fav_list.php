<?php 
include('constants.php');

# favourite list api for customer
$userId = $data['userId'];
$favouriteSpecialistDetails = array();
$specialistArray = array();
//echo $userId;die;
$favListQuery = @mysqli_query($con, "SELECT f.*,s.* FROM `favorite` f inner join specialist s on f.spe_id=s.spr_id WHERE f.user_id=$userId");
/*echo "SELECT f.*,s.* FROM `favorite` f inner join specialist s on f.spe_id=s.spr_id WHERE f.user_id=$userId";
die;*/
if (@mysqli_num_rows($favListQuery) > 0){

	while($favRow = @mysqli_fetch_array($favListQuery)){

			$specialistArray['specialist_fav_id'] = $favRow['fav_id'];		
			$specialistArray['specialist_brand_name'] = $favRow['brand_name'];
			$specialistArray['specialist_brand_logo'] = FAVOURITE_IMAGE_PATH.$favRow['brand_logo'];
			$specialistArray['specialist_contact'] = $favRow['contact'];
			$specialistArray['specialist_address'] = $favRow['address'];
			$specialistArray['specialist_city_name'] = $favRow['city_name'];

			$specialistArray['specialist_id'] = $favRow['spr_id'];

			$specialistArray['specialist_cat_id'] =	$favRow['cat_id'];
			$catId = $favRow['cat_id'];

			$specialistArray['specialist_subcat_id'] = $favRow['subcat_id'];
			$subcatId = $favRow['subcat_id'];

			
			// category name fetch starts here
			$categoryQuery = @mysqli_query($con,"SELECT c.name FROM `category` c WHERE c.cat_id=$catId");
			if(@mysqli_num_rows($categoryQuery) > 0){
				$categoryData = @mysqli_fetch_array($categoryQuery);
				$specialistArray['specialist_category_name'] = $categoryData['name'];		
			}else{
				$specialistArray['specialist_category_name'] = "NA";
			}		
			// category name fetch ends here

			// subcategory name fetch starts here
			$subCategoryQuery = @mysqli_query($con,"SELECT sc.subcat_name FROM `subcategory` sc WHERE sc.subcat_id=$subcatId");
			if(@mysqli_num_rows($subCategoryQuery) > 0){
				$subCategoryData = @mysqli_fetch_array($subCategoryQuery);
				$specialistArray['specialist_subcategory_name'] = $subCategoryData['subcat_name'];		
			}else{
				$specialistArray['specialist_subcategory_name'] = "NA";
			}		
			// subcategory name fetch ends here
			
			$specialistArray['specialist_contact_image'] = FAVOURITE_IMAGE_PATH.$favRow['contact_image'];
			
			
			$specialistArray['specialist_disc'] = $favRow['disc'];
			
			

			

			$specialistArray['specialist_zip_code'] = $favRow['zip_code'];
		


			# validation checks before sending response	
			if($specialistArray['specialist_brand_name'] == '' || $specialistArray['specialist_brand_name'] == null){
				$specialistArray['specialist_brand_name'] = 'NA';
			}
			if($specialistArray['specialist_brand_logo'] == '' || $specialistArray['specialist_brand_logo'] == null){
				$specialistArray['specialist_brand_logo'] = 'NA';
			}
			if($specialistArray['specialist_contact'] == '' || $specialistArray['specialist_contact'] == null){
				$specialistArray['specialist_contact'] = 'NA';
			}
			if($specialistArray['specialist_address'] == '' || $specialistArray['specialist_address'] == null){
				$specialistArray['specialist_address'] = 'NA';
			}
			if($specialistArray['specialist_city_name'] == '' || $specialistArray['specialist_city_name'] == null){
				$specialistArray['specialist_city_name'] = 'NA';
			}
			array_push($favouriteSpecialistDetails,$specialistArray);
	}

	// print_r($favouriteSpecialistDetails);die;

	$datatext['status'] = true;
	$datatext['message'] = "Successfully Listed.";
	$datatext['details'] = $favouriteSpecialistDetails;
	$logParameters = array(
			"Request_Remote_Address" => $remoteAddress,
			"Requested_Page" => 'Favourite_List',
			"Request_Method" => $requestType,
			"Request_Sent_From" => $deviceType,
			"Requested_Date_Time" => date('Y-m-d h:i:s'),
			"Request_Status" => 'success',
			"Actual_Data_Received" => $json,
			"Data_Responded" => $favouriteSpecialistDetails
	);
	$logs->create_log($logParameters,'customer');
}
else{
	$datatext['results'] = false;
	$datatext['message'] = "No Favourite List Found.";
	$datatext['details'] = [];
	$logParameters = array(
			"Request_Remote_Address" => $remoteAddress,
			"Requested_Page" => 'Favourite_List',
			"Request_Method" => $requestType,
			"Request_Sent_From" => $deviceType,
			"Requested_Date_Time" => date('Y-m-d h:i:s'),
			"Request_Status" => 'failed',
			"Actual_Data_Received" => $json,
			"Data_Responded" => "NA"
	);
	$logs->create_log($logParameters,'customer');
}
echo json_encode($datatext);

?>